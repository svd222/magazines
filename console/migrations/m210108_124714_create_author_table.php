<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author}}`.
 */
class m210108_124714_create_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%author}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'patronymic' => $this->string(),
            'surname' => $this->string()->notNull()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('idx_author_surname', '{{%author}}', 'surname');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author}}');
    }
}
