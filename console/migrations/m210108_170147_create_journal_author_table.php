<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%journal_author}}`.
 */
class m210108_170147_create_journal_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%journal_author}}', [
            'author_id' => $this->integer()->notNull(),
            'journal_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('author_journal', '{{%journal_author}}', ['author_id', 'journal_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%journal_author}}');
    }
}
