<?php

use yii\db\Migration;

/**
 * Class m210109_114919_change_journal_image_column_name
 */
class m210109_114919_change_journal_image_column_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('{{%journal}}', 'image', 'image_file');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%journal}}', 'image_file', 'image');
    }
}
