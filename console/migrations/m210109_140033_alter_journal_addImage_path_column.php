<?php

use yii\db\Migration;

/**
 * Class m210109_140033_alter_journal_addImage_path_column
 */
class m210109_140033_alter_journal_addImage_path_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%journal}}', 'image_path', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%journal}}', 'image_path');
    }
}
