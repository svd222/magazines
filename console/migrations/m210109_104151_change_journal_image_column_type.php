<?php

use yii\db\Migration;

/**
 * Class m210109_104151_change_journal_image_column_type
 */
class m210109_104151_change_journal_image_column_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%journal}}', 'image', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%journal}}', 'image', $this->binary());
    }
}
