<?php
namespace common\helpers;

use OutOfBoundsException;

class UploadError
{
    /**
     * @var string[] Describes a file upload error
     */
    private static $errors = [
        0 => 'There is no error, the file uploaded with success.',
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        3 => 'The uploaded file was only partially uploaded.',
        4 => 'No file was uploaded.',
        6 => 'Missing a temporary folder.',
        7 => 'Failed to write file to disk.',
        8 => 'A PHP extension stopped the file upload.',
    ];

    /**
     * Return error description
     *
     * @see https://www.php.net/manual/en/features.file-upload.errors.php
     * @param int $code
     * @throws OutOfBoundsException
     * @return string
     */
    public static function getErrorMessage(int $code = 0): string {
        if (($code >= 0 && $code <= 4) || ($code >= 6 && $code <= 8)) {
            return static::$errors[$code] ?? '';
        }
        throw new OutOfBoundsException('Incorrect error code');
    }
}
