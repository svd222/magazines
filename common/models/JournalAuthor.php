<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%journal_author}}".
 *
 * @property int $author_id
 * @property int $journal_id
 */
class JournalAuthor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%journal_author}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'journal_id'], 'required'],
            [['author_id', 'journal_id'], 'integer'],
            [['author_id', 'journal_id'], 'unique', 'targetAttribute' => ['author_id', 'journal_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'author_id' => Yii::t('journal_author', 'Author ID'),
            'journal_id' => Yii::t('journal_author', 'Journal ID'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return JournalAuthorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JournalAuthorQuery(get_called_class());
    }
}
