<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%author}}".
 *
 * @property int $id
 * @property string $name
 * @property string|null $patronymic
 * @property string $surname
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%author}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['name', 'patronymic'], 'string', 'max' => 255],
            [['surname'], 'string', 'min' => 3],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournalAuthors()
    {
        return $this->hasMany(JournalAuthor::class, ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournals()
    {
        return $this->hasMany(Journal::class, ['id' => 'journal_id'])->via('journalAuthors');
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('author', 'ID'),
            'name' => Yii::t('author', 'Name'),
            'patronymic' => Yii::t('author', 'Patronymic'),
            'surname' => Yii::t('author', 'Surname'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AuthorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AuthorQuery(get_called_class());
    }
}
