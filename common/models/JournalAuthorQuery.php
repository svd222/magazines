<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[JournalAuthor]].
 *
 * @see JournalAuthor
 */
class JournalAuthorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return JournalAuthor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return JournalAuthor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
