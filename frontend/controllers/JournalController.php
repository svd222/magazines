<?php

namespace frontend\controllers;

use common\helpers\UploadError;
use common\models\Author;
use common\models\JournalAuthor;
use Yii;
use common\models\Journal;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Journal models.
     *
     * @var $id integer Author id
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        if ($id) {
            $query = Journal::find()
                ->joinWith([
                    'authors' => function($query) use ($id) {
                        /**
                         * @var $query ActiveQuery
                         */
                        $query->where('{{author}}.id='.$id)->alias('author');
                    }
                ]);
        } else {
            $query = Journal::find()->joinWith('authors');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        if ($id) {
            $viewData = [
                'dataProvider' => $dataProvider,
                'author' => Author::findOne($id),
            ];
        } else {
            $viewData = [
                'dataProvider' => $dataProvider,
                'author' => null
            ];
        }
        return $this->render('index', $viewData);
    }

    /**
     * Displays a single Journal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, ['authors']),
        ]);
    }

    /**
     * Creates a new Journal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Journal();

        $isPostRequest = Yii::$app->request->isPost;
        $transactionResult = false;
        $post = Yii::$app->request->post();
        if ($isPostRequest && $model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            $modelName = $model->formName();
            $postData = Yii::$app->request->post($modelName);

            $image = UploadedFile::getInstance($model, 'image_file');
            if (!empty($image)) {
                if ($image->error) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', UploadError::getErrorMessage($image->error));
                    return $this->redirect('index');
                }
            } else {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('journal','Image was not loaded'));
                return $this->redirect('index');
            }

            $hash = substr(md5($image->baseName . '.' . $image->extension . time()), 0 , 16);
            $path = 'uploads/' . $hash . '.' . $image->extension;
            $image->saveAs($path);
            $model->image_path = $path;

            $journalSaved =  $model->save();
            if ($journalSaved) {
                $authorList = $postData['authorList'];

                $items = [];
                foreach ($authorList as $authorID) {
                    $items[] = [
                        $authorID, $model->id
                    ];
                }

                $linkCount = Yii::$app->db->createCommand()->batchInsert(
                    '{{%journal_author}}',
                    ['author_id', 'journal_id'],
                    $items
                )->execute();

                if ($linkCount) {
                    $transaction->commit();
                    $transactionResult = true;
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', Yii::t('journal', 'Something is wrong'));
                    return $this->redirect('index');
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('journal', 'Can`t save journal.'));
                Yii::error(var_export($model->errors, true));
                $transaction->rollBack();
                return $this->redirect('index');
            }
        }

        if ($isPostRequest && $transactionResult) {
            Yii::$app->session->setFlash('success', UploadError::getErrorMessage());
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $authors = Author::find()->all();

        return $this->render('create', [
            'model' => $model,
            'authors' => $authors
        ]);
    }

    /**
     * Updates an existing Journal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, ['authors']);

        $currentAuthorIds = ArrayHelper::getColumn($model->authors, 'id');

        $isPostRequest = Yii::$app->request->isPost;
        $transactionResult = false;

        if ($isPostRequest && $model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            $modelName = $model->formName();
            $postData = Yii::$app->request->post($modelName);

            $image = UploadedFile::getInstance($model, 'image_file');
            if (!empty($image)) {
                if ($image->error) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', UploadError::getErrorMessage($image->error));
                    return $this->redirect('index');
                }

                $hash = substr(md5($image->baseName . '.' . $image->extension . time()), 0 , 16);
                $path = 'uploads/' . $hash . '.' . $image->extension;
                $image->saveAs($path);
                $model->image_path = $path;
            }

            $journalSaved =  $model->save();
            if ($journalSaved) {
                //first, we need clear all links to authors associated with journal
                JournalAuthor::deleteAll(['and', ['in', 'author_id', $currentAuthorIds], ['journal_id' => $model->id]]);

                $authorList = $postData['authorList'];

                $items = [];
                foreach ($authorList as $authorID) {
                    $items[] = [
                        $authorID, $model->id
                    ];
                }

                if (!empty($items)) {
                    $linkCount = Yii::$app->db->createCommand()->batchInsert(
                        '{{%journal_author}}',
                        ['author_id', 'journal_id'],
                        $items
                    )->execute();

                    if ($linkCount) {
                        $transaction->commit();
                        $transactionResult = true;
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('error', Yii::t('journal', 'Something is wrong'));
                        return $this->redirect('index');
                    }
                } else {
                    $transaction->commit();
                    $transactionResult = true;
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('journal', 'Can`t save journal.'));
                Yii::error(var_export($model->errors, true));
                $transaction->rollBack();
                return $this->redirect('index');
            }
        }

        if ($isPostRequest && $transactionResult) {
            Yii::$app->session->setFlash('success', UploadError::getErrorMessage());
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $authors = Author::find()->all();

        $dropDownListOptions = [];
        foreach ($currentAuthorIds as $cID) {
            $dropDownListOptions[$cID] = ['selected' => true];
        }
        $dropDownListOptions = [
            'options' => $dropDownListOptions
        ];

        return $this->render('update', [
            'model' => $model,
            'authors' => $authors,
            'dropDownListOptions' => $dropDownListOptions,
            'isUpdate' => true,
        ]);
    }

    /**
     * Deletes an existing Journal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Journal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, array $with = [])
    {
        if (empty($with)) {
            if (($model = Journal::findOne($id)) !== null) {
                return $model;
            }
        } else {
            if (($model = Journal::find()->with($with)->where('[[id]]='.$id)->one()) !== null) {
                return $model;
            }
        }

        throw new NotFoundHttpException(Yii::t('journal', 'The requested page does not exist.'));
    }
}
