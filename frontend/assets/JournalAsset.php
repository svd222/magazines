<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class JournalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'lightbox/css/lightbox.min.css',
    ];

    public $js = [
        'lightbox/js/lightbox.min.js',
        'js/journal.js',
    ];

    public $depends = [
        AppAsset::class
    ];
}
