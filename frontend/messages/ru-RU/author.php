<?php
return [
    'ID' => 'ID',
    'Name' => 'Имя',
    'Patronymic' => 'Отчество',
    'Surname' => 'Фамилия',
    'Create Author' => 'Создать автора',
    'Authors' => 'Авторы',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Show journal list' => 'Показать список журналов',
    'Update Author: {name}' => 'Обновить автора: {name}',
    'authors magazines' => 'журналы автора',
];
