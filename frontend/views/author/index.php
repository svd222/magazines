<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('author', 'Authors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('author', 'Create Author'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'patronymic',
            'surname',
            [
                'class' => \yii\grid\DataColumn::class,
                'label' => Yii::t('author', 'authors magazines'),
                'content' => function ($model, $key, $index, $column) {
                    $span = '<span class="glyphicon glyphicon-book"></span>';
                    $content = Html::a($span, ['journal/index', 'id' => $model->id]);
                    return '<div class="journal_centered">' . $content . '</div>';
                },
                'headerOptions' => [
                    'class' => 'journal_header_centered',
                ],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
