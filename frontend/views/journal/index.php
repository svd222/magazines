<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\assets\JournalAsset;
use yii\helpers\Url;
use common\models\Journal;
use common\models\Author;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $author \common\models\Author|null */

$this->title = Yii::t('journal', 'Journals');
$this->params['breadcrumbs'][] = $this->title;
JournalAsset::register($this);
?>
<div class="journal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        if (!empty($author)) {
            ?>
                <h3>
                    <span class="author_journal_title">
                    <?= Yii::t('journal', 'List of author\'s journals: {name} {surname}',
                        ['name' => $author->name, 'surname' => $author->surname]) ?>
                    </span>
                </h3>
            <?php
        }
    ?>

    <p>
        <?= Html::a(Yii::t('journal', 'Create Journal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'content' => function ($model, $key, $index, $column) {
                    /**
                     * @var $model Journal
                     */
                    return Html::a($model->name, ['journal/view', 'id' => $model->id]);
                },
                'header' => Html::a(
                    Yii::t('journal', 'Name'),
                    Url::to(['journal/index', 'sort' => 'name']),
                    [
                        'data-sort' => '-name',
                        'class' => 'asc'
                    ]
                )
            ],
            [
                'header' => Html::a(Yii::t('journal', 'Description'), '#'),
                'label' => Yii::t('journal', 'description'),
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /**
                     * @var $model Journal
                     */
                    return $model->description;
                },
                'contentOptions'=>['style'=>'max-width: 250px;']
            ],
            [
                'label' =>  Yii::t('journal', 'authors'),
                'header' => Html::a(Yii::t('journal', 'authors'), '#'),
                'content' => function ($model, $key, $index, $column) {
                    $authorListStr = '';
                    foreach ($model->authors as $author) {
                        /**
                         * @var $author Author
                         */
                        $authorListStr .= $author->surname  . ' ' . $author->name . ' ' . $author->patronymic . '<br  />';
                    }
                    return $authorListStr;
                }
            ],
            [
                'header' => Yii::t('journal', 'Magazine cover'),
                'content' => function ($model, $key, $index, $column) {
                    $imageUrl = Yii::$app->request->hostInfo . DIRECTORY_SEPARATOR . $model->image_path;
                    $image = '<img src="' . $imageUrl . '" width="100" height="100" alt="">';
                    return Html::a($image, $imageUrl, [
                        'class' => 'magazine_cover',
                        'data-lightbox' => 'image',
                    ]);
                }
            ],
            'issue_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
