<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Journal */
/* @var $form yii\widgets\ActiveForm */
/* @var $authors common\models\Author[] */
/* @var $dropDownListOptions array|null */
/* @var $isUpdate bool|null */

$authorList = [];
foreach ($authors as $author) {
    $authorList[$author->id] = $author->surname . ' ' . $author->name . ' ' . $author->patronymic;
}
$options = [
    'prompt' => 'Выберите автора...',
    'multiple' => true,
];
if (!empty($dropDownListOptions)) {
    $options = ArrayHelper::merge($options, $dropDownListOptions);
}
$isUpdate = $isUpdate ?? false;
?>

<div class="journal-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                </div>

            </div>

            <div class="row">

                <div class="col-md-5">

                    <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'rows' => 5, 'cols' => 7]) ?>

                </div>

            </div>

            <?php if (!$isUpdate) { ?>
                <div class="row">

                    <div class="col-md-3">

                        <?= $form->field($model, 'image_file')->fileInput() ?>

                    </div>

                </div>
            <?php } else { ?>
                <div class="row">

                    <div class="col-md-3">

                        <?php
                            $imageUrl = Yii::$app->request->hostInfo . DIRECTORY_SEPARATOR . $model->image_path;
                            $image = '<img src="' . $imageUrl . '" width="100" height="100" alt="">';
                            echo Html::a($image, $imageUrl, [
                                'class' => 'magazine_cover',
                                'data-lightbox' => 'image',
                            ]);
                        ?>

                    </div>

                </div>
            <?php } ?>

            <div class="row">

                <div class="col-md-3 mt10">
                    <div><?= Yii::t('journal', 'Issue Date') ?></div>
                    <?php
                        echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'issue_date',
                        'language' => 'ru',
                        'dateFormat' => 'yyyy-MM-dd',
                        ]);
                    ?>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">

                    <?= $form->field($model, 'authorList')->dropDownList($authorList, $options) ?>

                </div>

            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('journal', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

        </div>

    <?php ActiveForm::end(); ?>

</div>
