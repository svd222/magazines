<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\assets\JournalAsset;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Journal */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('journal', 'Journals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
JournalAsset::register($this);
?>
<div class="journal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('journal', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('journal', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('journal', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $authors = $model->authors;
        $attributes = [
            'id',
            'name',
            'description',
            'issue_date',
        ];
        $authorsAttribute = [[
            'label' => Yii::t('journal', 'Authors'),
            'value' => function($model, $widget) use ($authors) {
                $authors = array_map(
                    function($n, $p, $s) {
                        return '<div>' . $n . ' ' . $p . ' ' . $s . '</div>';
                    },
                    ArrayHelper::getColumn($authors, 'name'),
                    ArrayHelper::getColumn($authors, 'patronymic'),
                    ArrayHelper::getColumn($authors, 'surname')
                );

                return implode(' ', $authors);
            },
            'format' => 'raw',
        ]];
        $attributes = ArrayHelper::merge($attributes, $authorsAttribute);
        if ($model->image_path) {
            $imageAttribute = [[
                'attribute' => 'image_path',
                'value' => function($model, $widget) {
                    $imageUrl = Yii::$app->request->hostInfo . DIRECTORY_SEPARATOR . $model->image_path;
                    $image = '<img src="' . $imageUrl . '" width="100" height="100" alt="" class="magazine_cover">';
                    return Html::a($image, $imageUrl, [
                        'class' => 'magazine_cover',
                        'data-lightbox' => 'image',
                    ]);
                },
                'format' => 'raw',
            ]];
            $attributes = ArrayHelper::merge($attributes, $imageAttribute);
        }
        /*var_export($attributes);
        Yii::$app->end();*/
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
