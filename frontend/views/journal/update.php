<?php

use yii\helpers\Html;
use frontend\assets\JournalAsset;

/* @var $this yii\web\View */
/* @var $model common\models\Journal */
/* @var $authors common\models\Author[] */
/* @var $dropDownListOptions array|null */
/* @var $isUpdate bool */

JournalAsset::register($this);

$dropDownListOptions = $dropDownListOptions ?? [];

$this->title = Yii::t('journal', 'Update Journal: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('journal', 'Journals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('journal', 'Update');
?>
<div class="journal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'authors' => $authors,
        'dropDownListOptions' => $dropDownListOptions,
        'isUpdate' => $isUpdate,
    ]) ?>

</div>
