## INSTALLATION & CONFIGURATION

### INSTALLATION

```
cd /path/to/project
git clone git@bitbucket.org:svd222/magazines.git .
composer install
php yii init
```
After that be sure that directory frontend/web/uploads exists. This directory intended for uploaded files.
This directory should have appropriate user rights. If not exists then you should create it.

Next we goin to configurate DB, for that
open common/config/main-local.php and fill appropriate configuration of database component. For example:
``` 
'db' => [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=DATABASE',
    'username' => 'USER',
    'password' => 'PASSWORD',
    'charset' => 'utf8',
    'tablePrefix' => 'j_',
],
```

Next open file /etc/hosts and add your local domain:
```
127.0.0.1 journal.local
```
Possible you need increase upload max file size. For that add this lines to your php.ini config file
```
upload_max_filesize = 2M
post_max_size = 2M
```
### NGINX CONFIGURATION EXAMPLE
```
 server {
	listen 80;
	sendfile        on;
	keepalive_timeout  65;
    gzip  on;
    gzip_min_length 1024;
    gzip_buffers 12 32k;
    gzip_comp_level 9;
    gzip_proxied any;
	gzip_types	text/plain application/xml text/css text/js text/xml application/x-javascript text/javascript application/javascript application/json application/xml+rss;
	
	server_name .journal.local;
	root /var/www/journal/frontend/web;

	access_log /var/www/journal/access.log;
	error_log /var/www/journal/error.log;

	charset utf-8;
		
	location / {
	    root /var/www/journal/frontend/web/;
	    index index.php;
	    try_files $uri $uri/ /index.php?$args;
	}

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        
        root /var/www/journal/frontend/web/;
    
        try_files $uri $uri/ =404;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param                   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param QUERY_STRING    $query_string;
        fastcgi_param REQUEST_METHOD  $request_method;
        fastcgi_param CONTENT_TYPE    $content_type;
        fastcgi_param CONTENT_LENGTH  $content_length;
        fastcgi_param  PATH_INFO $fastcgi_path_info;
    }
}
```
Restart web server & php-fpm
```
 sudo service nginx restart
 sudo service php7.3-fpm restart  
```
after that project should be available at: http://journal.local
